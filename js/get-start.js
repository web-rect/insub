

const $regionJs = document.querySelector('.region-js'),
$regionJsP = document.querySelector('.region-js p'),
$phoneNum = document.querySelector('.phone-num .num'),
$phoneNumInput = document.querySelector('.phone-num .phone'),
$searchClearJs = document.querySelector('.search-clear-js'),
$boxItemJs1 = document.querySelector('.box-item-js-1'),
$boxRegionJs2 = document.querySelector('.box-region-js-2'),
$listRegionWrap = document.querySelector('.list-region-wrap'),
$firstRow = document.querySelectorAll('.list-region-wrap .first-row'),
$inputFile = document.querySelector('.input-file'),
$imgFile = document.querySelector('.img-file'),
$btnJsInfo = document.querySelector('.btn-js-info'),
$btnStartJs = document.querySelector('.btn-start-js'),
$telJs = document.querySelector('.tel-js'),
$yourInfo = document.querySelector('.your-info'),
$inputYou = [...document.querySelectorAll('.input-you')],
$formSearchChats = document.querySelector('.form-search-chats'),
$inputSearch = document.querySelector('.form-search-chats input'),
$inputP = document.querySelector('.form-search-chats p'),
$inputImg = document.querySelector('.form-search-chats img'),
$searchTitleJs = document.querySelector('.search-title-js'),
$searchList = document.querySelector('.search-list'),
$getStarted = document.querySelector('.get-started'),
$pincodeJs = document.querySelector('.pincode-js'),
$formPin = document.querySelector('.form-pin'),
$formPinInput = document.querySelector('.form-pin input'),
$sectionMain = document.querySelector('.section-main'),
$leave = document.querySelector('.leave'),
$chatJs = document.querySelector('.chat-js'),
$fixMaterialJs = document.querySelector('.fix-material-js'),
$broochJs = document.querySelector('.brooch-js'),
$closeFixMaterial = document.querySelector('.close-fix-material'),
$allHrefLink = document.querySelectorAll("a[href^='http']"),
$openLinkJs = document.querySelector('.open-link-js'),
$openLinkWrapperP = document.querySelector('.open-link-wrapper p'),
$btnCancelLink = document.querySelector('.btn-cancel-link'),
$listUserSearchWrapper = document.querySelector('.list-user-search-wrapper'), 
$listUserSearchWrapperItem = [...document.querySelectorAll('.list-user-search-wrapper .item input')],
$btnShare = document.querySelector('.btn-share'),
$shareSearchJs = document.querySelector('.share-search-js'),
$shareSearchJsInput = document.querySelector('.share-search-js input'),
$contactsSearchJs = document.querySelector('.contacts-search-js'),
$contactsSearchJsInput = document.querySelector('.contacts-search-js input'),
$cancelSearchJs = document.querySelector('.Cancel-search-js'),
$cancelSearchJsContacts = document.querySelector('.Cancel-search-js-contacts'),
$imgSearchShareLeft = document.querySelector('.img-search-share-left'),
$spanSearchShareLeft = document.querySelector('.span-search-share-left'),
$imgSearchContactsLeft = document.querySelector('.img-search-contacts-left'),
$spanSearchContactsLeft = document.querySelector('.span-search-contacts-left'),
$clearSearchInput = document.querySelector('.clear-search-input'),
$clearSearchInputContacts = document.querySelector('.clear-search-input-contacts'),
$profile3rdPartyjs = document.querySelector('.profile-3rd-party-js'),
$userNameTitleJs = document.querySelector('.user-name-title-js'),
$backJsInfo = document.querySelector('.back-js-info'),
$backNotifaiJs = document.querySelector('.back-notifai-js'),
$wrapperInfoJs = document.querySelector('.wrapper-info'),
$editJsInfo = document.querySelector('.edit-js-info'),
$wrapperEditJs = document.querySelector('.wrapper-edit'),
$cancelJsEdit = document.querySelector('.cancel-js-edit'),
$doneJsEdit = document.querySelector('.done-js-edit'),
$notificationsOpenJs = document.querySelector('.notifications-open-js'),
$sharedOpenJs = document.querySelector('.shared-open-js-3d'),
$wrapperNotificationsJs = document.querySelector('.wrapper-notifications-js'),
$wrapperShareJs = document.querySelector('.wrapper-share-js-3d'),
$backShareJs = document.querySelector('.back-share-js'),
$settingsJs = document.querySelector('.settings-js'),
$settingsJsA = document.querySelectorAll('.settings-js a'),
$chatsUsersJs = document.querySelector('.chats-users-js'),
$contactsUserAddJs = document.querySelector('.contacts-user-add-js'),
$setProfile = document.querySelectorAll('.set-profile'),
$addPlusContacts = document.querySelector('.add-plus-contacts'),
$addedContactsJs = document.querySelector('.added-contacts-js'),
$backContactsJs = document.querySelector('.back-contacts-js'),
$searchListContactsSend = document.querySelector('.search-list-contacts-send'),
$typeSendMessageJs = document.querySelector('.type-send-message-js'),
$bgProfile = document.querySelector('.bg-profile-js'),
$shareMyProfileJs = document.querySelector('.share-my-profile-js'),
$editJsInfoProfile = document.querySelector('.edit-js-info-profile'),
$editedProfileUserJs = document.querySelector('.edited-profile-user-js'),
$backProfileUserJs = document.querySelector('.back-profile-user-js'),
$doneStatusProfileUser = document.querySelector('.done-status-profile-user'),
$soundsAndNotificationsJs = document.querySelector('.sounds-and-notifications-js'),
$editedProfileSoundsJs = document.querySelector('.edited-profile-sound-js'),
$backSoundsProfileJs = document.querySelectorAll('.back-chekers-profile-js'),
$editedProfilePrivacyJs = document.querySelector('.edited-profile-privacy-js'),
$privacyAndSecurityJs = document.querySelector('.privacy-and-security-js'),
$dataAndStorageJs = document.querySelector('.data-and-storage-js'),
$editedProfileStorageJs = document.querySelector('.edited-profile-storage-js'),
$profileInfoJs = document.querySelector('.profile-info-js'),
$scrollDwn = document.querySelector('.scroll-dwn'),
$chatBodyWrapperScroll = document.querySelector('.chat-body-wrapper--scroll'),
$chatBodyJs = document.querySelector('.chat-body-js');
 




// открытие региона

$regionJs.addEventListener('click', () => {
    $boxItemJs1.classList.add('activeLeft');
    setTimeout(() => {
        $boxRegionJs2.classList.add('active');
    }, 200);
    regionThis();
});

// закрытие региона и выбора его

function regionThis() {
    $firstRow.forEach(function(item) {
        item.addEventListener('click', () => {
            let $name = item.querySelector('p').textContent;
            let $num = +item.querySelector('p b').textContent;
            $boxItemJs1.classList.remove('activeLeft');
            setTimeout(() => {
                $boxRegionJs2.classList.remove('active');
            }, 200);
         
            $regionJsP.textContent = $name;
            $phoneNum.textContent = `+${$num}`;
        });
    });
};

// чистка поле поиска

$searchClearJs.addEventListener('click', () => {
    $phoneNumInput.value = '';
});

// загрузка картинки

$inputFile.addEventListener('change' , function() {
    if (this.files && this.files[0]) {
        let read = new FileReader();
        read.onload = function(e) {
            $imgFile.setAttribute('src', e.target.result);
        }
        read.readAsDataURL(this.files[0]);
    }
});

// закрытие модалки ваша информация

$btnJsInfo.addEventListener('click', (e) => {
    e.preventDefault();
    if ($inputYou[0].value.length >= 2 && $inputYou[1].value.length >= 2 && $inputYou[2].value.length >= 2) {
        $yourInfo.classList.remove('active');
        setTimeout(() => {
            $yourInfo.classList.remove('active-flex');
            $sectionMain.classList.add('active');
        }, 200);
    }

    
});




// поиск чата людей левом меню

const $cancelSearchJsChats = document.querySelector('.cancel-search-js-chats-s'),
$spanSearchChatsLeft = document.querySelector('.span-search-chats-left'),
$imgSearchChatsLeft = document.querySelector('.img-search-chats-left'),
$clearSearchInputChats = document.querySelector('.clear-search-input-chats'),
$formSearchChatsInput = document.querySelector('.form-search-chats input'),
$formSearchChatsActive = document.querySelector('.form-search-chats');


document.body.addEventListener('click', function(e) {
 
    if (!$inputSearch.value.length >= 1 ) {

        if (e.target.closest('.search-list') || e.target.closest('.form-search-chats input') ) {
            $searchTitleJs.classList.add('active');
            $searchList.classList.add('active');
        
            $cancelSearchJsChats.classList.add('active');
            $imgSearchChatsLeft.classList.add('active');
            $spanSearchChatsLeft.classList.add('active');
            $clearSearchInputChats.classList.add('active');
            $searchListContactsSend.classList.add('active');
            $contactsSearchJs.classList.add('active');
            $formSearchChatsActive.classList.add('active');
            

        } else {
            $inputImg.style.display = '';
            $searchTitleJs.classList.remove('active');
            $searchList.classList.remove('active');
            $imgSearchChatsLeft.classList.remove('active');
            $spanSearchChatsLeft.classList.remove('active');
            $clearSearchInputChats.classList.remove('active');
            $searchListContactsSend.classList.remove('active');
            $contactsSearchJs.classList.remove('active');
            $cancelSearchJsChats.classList.remove('active');
            $formSearchChatsActive.classList.remove('active');
        }

    } 

});

$clearSearchInputChats.addEventListener('click', () => {
    setTimeout(() => {
        $formSearchChatsInput.value = '';
        $formSearchChatsInput.focus();
    }, 50);


});
$cancelSearchJsChats.addEventListener('click', () => {
    setTimeout(() => {
        $formSearchChatsInput.value = '';
        $cancelSearchJsChats.classList.remove('active');
        $imgSearchChatsLeft.classList.remove('active');
        $spanSearchChatsLeft.classList.remove('active');
        $clearSearchInputChats.classList.remove('active');
        $searchTitleJs.classList.remove('active');
        $searchList.classList.remove('active');
    }, 50);

});

// поиск чата все

const $addedChatsAll = document.querySelectorAll('.added-chats-all'),
$clearSearchInputChatsAll = document.querySelectorAll('.added-chats-clear-all'),
$formSearchChatsInputAll = document.querySelectorAll('.form-search-chats-all input'),
$formSearchChatsActiveAll = document.querySelectorAll('.form-search-chats-all'),
$backAll = document.querySelectorAll('.back-all'),
$modalAllS = document.querySelectorAll('.modal-all-s'),
$rowUserItem =  document.querySelector('.row-user-item'),
$rowUserItemIt =  [...document.querySelectorAll('.pb-group-pole.active .row-user-item .user-it')],
$pbGroupPole = document.querySelector('.pb-group-pole');



document.body.addEventListener('click', function(e) {

    $formSearchChatsInputAll.forEach(item => {

        if (!item.value.length >= 1) {
            
            if ($rowUserItemIt.length >= 1) {
                setTimeout(() => {
                    
                    $addedChatsAll.forEach(item => {
                        item.classList.add('active');;
                    });
                }, 0);
               
            }
            if (e.target.closest('.form-search-chats-all input') ) {
                
                $addedChatsAll.forEach(item => {
                    item.classList.add('active');
                });
    
            } else {
                // $addedChatsAll.forEach(item => {
                //     item.classList.remove('active');
                // });
            }
    
        } else {
            $addedChatsAll.forEach(item => {
                item.classList.add('active');
            });
            
        }
    });


});
$clearSearchInputChatsAll.forEach(item => {
    item.addEventListener('click', () => {
        setTimeout(() => {
            $formSearchChatsInputAll.forEach(item => {
                item.value = '';
            });
            $addedChatsAll.forEach(item => {
                item.classList.remove('active');
            });
            $rowUserItem.innerHTML = '';
        }, 50);
    
    
    });
});

$addedChatsAll.forEach(item => {
    item.addEventListener('click', () => {
        setTimeout(() => {
            $formSearchChatsInputAll.forEach(item => {
                item.value = '';
            });
            $addedChatsAll.forEach(item => {
                item.classList.remove('active');
            });
            $rowUserItem.innerHTML = '';
        }, 50);
    
    });
});

$backAll.forEach(item => {
    item.addEventListener('click', (e) => {
        e.preventDefault();
        $modalAllS.forEach(modal => {
            modal.classList.remove('active');
        });
    });
});



// подсчет количество выбранных юзеров 

const $blockUsersItemsJs = document.querySelectorAll(".block-users-items-js-all "),
$subnameJsGroup = document.querySelectorAll('.subname-js-group');

$blockUsersItemsJs.forEach(item => {
    item.addEventListener('click', () => {
        const $blockUsersItemsJsAll = item.querySelectorAll(".block-users-items-js-all input[type='checkbox']:checked").length;
    
        $subnameJsGroup.forEach(elem => {
            elem.textContent = `${$blockUsersItemsJsAll}/100000 `;
        })
    });
})



// старт

$btnStartJs.addEventListener('click', (e) => {
    e.preventDefault();
    if ($phoneNumInput.value.length >= 1) {
        $telJs.textContent = `${$phoneNum.textContent} ${$phoneNumInput.value} `;
        setTimeout(() => {
            $pincodeJs.classList.add('active');
        }, 200);
     
        $getStarted.classList.remove('active');
    
    }

});

$leave.addEventListener('click', () => {
    setTimeout(() => {
        $getStarted.classList.add('active');
       
    }, 200);
 
    $pincodeJs.classList.remove('active');

});

// проверка на количество символов вода пин кода

$formPinInput.onkeypress = (function() {
    let count = 0;
    return function(e) {
        let value = $formPinInput.value.split(" ");
        let lastValue = value[value.length - 1];
        count = lastValue.length + 1;

        if ($formPinInput.value === '1111') {
            $pincodeJs.classList.remove('active');
            $yourInfo.classList.add('active-flex');
            setTimeout(() => {
                $yourInfo.classList.add('active');
            }, 200);
        }

        if (e.keyCode == 32) count = 0;
        if (count >= 5) return false;
    };

})();

// закреп для сообщений ( открыть )

$broochJs.addEventListener('click', () => {
    $fixMaterialJs.classList.toggle('active');
    $chatJs.classList.toggle('blur-chat');
});

$chatJs.addEventListener('click', (e) => {
    if (!e.target.closest('.fix-material-js')) {
        $fixMaterialJs.classList.remove('active');
        $chatJs.classList.remove('blur-chat');
        $openLinkJs.classList.remove('active');

    }
    if (!e.target.closest('.click-i-mess')) {
        $blockMessAll.forEach(item => {
            item.style.zIndex = '';
        });
        $clickIMess.classList.remove('active');
    } else {
        $clickIMess.classList.add('active');
        $chatJs.classList.add('blur-chat');
    }
    
});

$closeFixMaterial.addEventListener('click', (e) => {
    e.preventDefault();
    $fixMaterialJs.classList.remove('active');
    $chatJs.classList.remove('blur-chat');
});
$btnCancelLink.addEventListener('click', (e) => {
    e.preventDefault();
    $openLinkJs.classList.remove('active');
    $chatJs.classList.remove('blur-chat');
});

// высота поля ввода 

const $enterTextareaMess = document.querySelectorAll('.en-mess-textarea');
const calcHeightMessEnter = () => {
    setTimeout(() => {
        $enterTextareaMess.forEach(item => {
            const $paddingTextMess = item.offsetHeight - item.clientHeight;

            item.style.height = item.scrollHeight + $paddingTextMess  + 'px';
            
            item.addEventListener('input', e => {
            
                item.style.height = 'auto';
                item.style.height = item.scrollHeight + $paddingTextMess  + 'px';
            });
        });
    }, 500);

}


// клик по всем ссылкам

const openAllLinkHref = function() {
    
    $allHrefLink.forEach(function(item){
        
        item.addEventListener('click', (e) => {
            e.preventDefault();
            setTimeout(() => {
                $openLinkJs.classList.add('active');
                $chatJs.classList.add('blur-chat');
            }, 50);
      

            $openLinkWrapperP.textContent = e.target.textContent;
        });
    });
};
openAllLinkHref();


// выбираем пользователей с поиска с кем поделиться Share

$listUserSearchWrapper.addEventListener('click', (e) => {
   
    for (let i = 0; i < $listUserSearchWrapperItem.length; i++) {
        const element = $listUserSearchWrapperItem[i];
        
        if (!$listUserSearchWrapperItem[i].checked >= 1) {
            $btnShare.classList.remove('active');
            
        } else {
            setTimeout(() => {
                $btnShare.classList.add('active');
            }, 50);
           
        }
        
    }
});

// открытие профиля человека 

const $backJsInfo3d = document.querySelector('.back-js-info-3d');

$userNameTitleJs.addEventListener('click', (e) => {
    e.preventDefault();
    $profile3rdPartyjs.classList.toggle('active');

    $wrapperInfoJs.classList.toggle('active-flex');
    setTimeout(() => {
        $wrapperInfoJs.classList.toggle('active');
    }, 200);
 
});


$backJsInfo3d.addEventListener('click', (e) => {
    e.preventDefault();
    $profile3rdPartyjs.classList.remove('active');
    setTimeout(() => {
        $wrapperInfoJs.classList.toggle('active-flex');
    }, 200);

    $wrapperInfoJs.classList.remove('active');
});
$editJsInfo.addEventListener('click', (e) => {
    e.preventDefault();
    $wrapperInfoJs.classList.remove('active');
    $wrapperEditJs.classList.add('active-flex');
    setTimeout(() => {
        $wrapperEditJs.classList.add('active');
    }, 200);
});
$cancelJsEdit.addEventListener('click', (e) => {
    e.preventDefault();
    $wrapperInfoJs.classList.add('active');
    $wrapperEditJs.classList.remove('active');
        setTimeout(() => {
        $wrapperEditJs.classList.remove('active-flex');
    }, 200);
});
$doneJsEdit.addEventListener('click', (e) => {
    e.preventDefault();
    $wrapperInfoJs.classList.add('active');
    $wrapperEditJs.classList.remove('active');
        setTimeout(() => {
        $wrapperEditJs.classList.remove('active-flex');
    }, 200);
});
$notificationsOpenJs.addEventListener('click', (e) => {
    e.preventDefault();
    $wrapperInfoJs.classList.remove('active');
    $wrapperNotificationsJs.classList.add('active-flex');
    setTimeout(() => {
        $wrapperNotificationsJs.classList.add('active');
    }, 200);
    
});
$backNotifaiJs.addEventListener('click', (e) => {
    e.preventDefault();
    $wrapperInfoJs.classList.add('active');
    $wrapperNotificationsJs.classList.remove('active-flex');
    setTimeout(() => {
        $wrapperNotificationsJs.classList.remove('active');
    }, 200);
    
});
$sharedOpenJs.addEventListener('click', (e) => {
    e.preventDefault();
    $wrapperInfoJs.classList.remove('active');
    $wrapperShareJs.classList.add('active');
    
});
$backShareJs.addEventListener('click', (e) => {
    e.preventDefault();
    $wrapperInfoJs.classList.add('active');
    $wrapperShareJs.classList.remove('active');
    
});


// показывать удаление share поиск 


$shareSearchJsInput.addEventListener('focus', () => {
    if ($shareSearchJsInput.value.length >= 0) {
        $cancelSearchJs.classList.add('active');
        $imgSearchShareLeft.classList.add('active');
        $spanSearchShareLeft.classList.add('active');
        $clearSearchInput.classList.add('active');

    } else {

        $imgSearchShareLeft.classList.remove('active');
        $spanSearchShareLeft.classList.remove('active');
        $clearSearchInput.classList.remove('active');
    }
});
$shareSearchJsInput.addEventListener('blur', () => {
    if ($shareSearchJsInput.value.length >= 1) {

        $imgSearchShareLeft.classList.add('active');
        $spanSearchShareLeft.classList.add('active');
        $clearSearchInput.classList.add('active');

    } else {
        $cancelSearchJs.classList.remove('active');
        $imgSearchShareLeft.classList.remove('active');
        $spanSearchShareLeft.classList.remove('active');
        $clearSearchInput.classList.remove('active');
    }
});

$clearSearchInput.addEventListener('click', () => {
    $shareSearchJsInput.value = '';
    $shareSearchJsInput.focus();

});
$cancelSearchJs.addEventListener('click', () => {
    $shareSearchJsInput.value = '';
    $cancelSearchJs.classList.remove('active');
    $imgSearchShareLeft.classList.remove('active');
    $spanSearchShareLeft.classList.remove('active');
    $clearSearchInput.classList.remove('active');
});


// показывать и удаление Conctacts поиск 

document.body.addEventListener('click', function(e) {
 
    if (!$contactsSearchJsInput.value.length >= 1 ) {
      
        if (e.target.closest('.search-list-contacts-send') || e.target.closest('.contacts-search-js input') ) {
            
            $cancelSearchJsContacts.classList.add('active');
            $imgSearchContactsLeft.classList.add('active');
            $spanSearchContactsLeft.classList.add('active');
            $clearSearchInputContacts.classList.add('active');
            $searchListContactsSend.classList.add('active');
            $contactsSearchJs.classList.add('active');
            

        } else {
            
            $imgSearchContactsLeft.classList.remove('active');
            $spanSearchContactsLeft.classList.remove('active');
            $clearSearchInputContacts.classList.remove('active');
            $searchListContactsSend.classList.remove('active');
            $contactsSearchJs.classList.remove('active');
            $cancelSearchJsContacts.classList.remove('active');
       
        }

    } 

});

$clearSearchInputContacts.addEventListener('click', () => {
    setTimeout(() => {
        $contactsSearchJsInput.value = '';
        $contactsSearchJsInput.focus();
    }, 50);


});
$cancelSearchJsContacts.addEventListener('click', () => {
    setTimeout(() => {
        $contactsSearchJsInput.value = '';
        $cancelSearchJsContacts.classList.remove('active');
        $imgSearchContactsLeft.classList.remove('active');
        $spanSearchContactsLeft.classList.remove('active');
        $clearSearchInputContacts.classList.remove('active');
        $searchListContactsSend.classList.remove('active');
        $contactsSearchJs.classList.remove('active');
    }, 50);

});



// переключение chats / contacts  / profile

$settingsJs.addEventListener('click', (e) => {
    e.preventDefault();
    $settingsJsA.forEach(item => {
        item.classList.remove('active');
    });
    $setProfile.forEach(elem => {
        elem.classList.remove('active');
        if (e.target.closest('.box').dataset.set === elem.dataset.set) {
            e.target.closest('.box').classList.add('active');
            elem.classList.add('active');
        }
    });
});

// вызов модалки добавить контакты и закрытие

$addPlusContacts.addEventListener('click', (e) => {
    e.preventDefault();
    $addedContactsJs.classList.add('active-flex');
    setTimeout(() => {
        $addedContactsJs.classList.add('active');
    }, 200);
});

$backContactsJs.addEventListener('click', (e) => {
    e.preventDefault();
    $addedContactsJs.classList.remove('active');
    setTimeout(() => {
        $addedContactsJs.classList.remove('active-flex');
    }, 200);

});

// profle открытие share 

$typeSendMessageJs.addEventListener('click', (e) => {
    e.preventDefault();
    $bgProfile.classList.add('active');
    $shareMyProfileJs.classList.add('active-block');
    setTimeout(() => {
        $shareMyProfileJs.classList.add('active');
    }, 150);

});
$bgProfile.addEventListener('click', (e) => {
    e.preventDefault();
    $shareMyProfileJs.classList.remove('active');
    $bgProfile.classList.remove('active');
    setTimeout(() => {
        $shareMyProfileJs.classList.remove('active-block');
    }, 150);

})


// редактирование профиля / открытие разделов профиля

$editJsInfoProfile.addEventListener('click', (e) => {
    e.preventDefault();

    $editedProfileUserJs.classList.add('active-flex');
    calcHeightMessEnter();
    $profileInfoJs.classList.remove('active');
    setTimeout(() => {
        $editedProfileUserJs.classList.add('active');
        $profileInfoJs.classList.remove('active-flex');
    }, 200);
});
$backProfileUserJs.addEventListener('click', (e) => {
    e.preventDefault();
    $editedProfileUserJs.classList.remove('active');
    $profileInfoJs.classList.add('active-flex');
    setTimeout(() => {
        $editedProfileUserJs.classList.remove('active');
        $profileInfoJs.classList.add('active');
    }, 200);
});
$doneStatusProfileUser.addEventListener('click', (e) => {
    e.preventDefault();
    $editedProfileUserJs.classList.remove('active');
    $profileInfoJs.classList.add('active-flex');

    setTimeout(() => {
        $editedProfileUserJs.classList.add('active');
        $profileInfoJs.classList.add('active');
    }, 200);
});


$soundsAndNotificationsJs.addEventListener('click', (e) => {
    e.preventDefault();
    $editedProfileSoundsJs.classList.add('active-flex');

    $profileInfoJs.classList.remove('active');
    setTimeout(() => {
        $editedProfileSoundsJs.classList.add('active');
        $profileInfoJs.classList.remove('active-flex');
    }, 200);
});
$privacyAndSecurityJs.addEventListener('click', (e) => {
    e.preventDefault();
    $editedProfilePrivacyJs.classList.add('active-flex');
    $profileInfoJs.classList.remove('active');
    setTimeout(() => {
        $editedProfilePrivacyJs.classList.add('active');
        $profileInfoJs.classList.remove('active-flex');
    }, 200);
});
$dataAndStorageJs.addEventListener('click', (e) => {
    e.preventDefault();
    $editedProfileStorageJs.classList.add('active-flex');
    $profileInfoJs.classList.remove('active');
    setTimeout(() => {
        $editedProfileStorageJs.classList.add('active');
        $profileInfoJs.classList.remove('active-flex');
    }, 200);
});

$backSoundsProfileJs.forEach(item => {
    item.addEventListener('click', (e) => {
        e.preventDefault();
        $editedProfileSoundsJs.classList.remove('active-flex');
        $editedProfilePrivacyJs.classList.remove('active-flex');
        $editedProfileStorageJs.classList.remove('active-flex');
        $profileInfoJs.classList.add('active-flex');

        setTimeout(() => {
            $editedProfileSoundsJs.classList.remove('active');
            $editedProfilePrivacyJs.classList.remove('active');
            $editedProfileStorageJs.classList.remove('active');
            $profileInfoJs.classList.add('active');
        }, 200);
    });
});


// скролл в низ чата 

$scrollDwn.addEventListener('click', (e) => {
    e.preventDefault();

    $chatBodyJs.scrollBy({
        top: $chatBodyJs.scrollHeight,
        behavior: 'smooth'
    });


});


// вызов меню по клику на сообщение ( пкм )

const $clickIMess = document.querySelector('.click-i-mess'),
$blockMessAll = document.querySelectorAll('.block-mess');

document.addEventListener('contextmenu', e => {
    e.preventDefault();

    // получение позиций
    const $blockMess = e.target.closest('.block-mess');
    const $blockMessHe = e.target.closest('.i-mess.he .block-mess');
    const $blockMessYou = e.target.closest('.i-mess.you .block-mess');

    $clickIMess.style.borderRadius = '20px';
    if ($blockMess) {
        setTimeout(() => {
            const $clickPopPosition = $blockMess.getBoundingClientRect().top;
         
            if ($blockMessHe) {
                $clickIMess.style.left = $blockMessHe.offsetLeft + $blockMessHe.offsetWidth - $clickIMess.offsetWidth + 'px';
                $clickIMess.style.top = $blockMessHe.offsetTop + $blockMessHe.offsetHeight  + 'px';
                $clickIMess.style.borderTopRightRadius = '5px';
                setTimeout(() => {
                
                    $chatBodyJs.scrollBy({
                        top:  ($clickPopPosition - $clickIMess.offsetHeight) + $blockMessHe.offsetHeight,
                        behavior: 'smooth'
                    });
            
                }, 50);
               
            }
            if ($blockMessYou) {
                $clickIMess.style.left = $blockMessYou.offsetLeft + 'px';
                $clickIMess.style.top = $blockMessYou.offsetTop + $blockMessYou.offsetHeight   + 'px';
                $clickIMess.style.borderTopLeftRadius = '5px';
                
                setTimeout(() => {
                    
                    $chatBodyJs.scrollBy({
                        top:  ($clickPopPosition - $clickIMess.offsetHeight) + $blockMessYou.offsetHeight ,
                        behavior: 'smooth'
                    });
                }, 50);
              
            }
    
        }, 150);
    
        if ($blockMess) {
            $chatJs.classList.add('blur-chat');
            $clickIMess.classList.add('active');
            $blockMess.style.zIndex = '544';
        }
    }


});

$clickIMess.addEventListener('click', e => {
    e.preventDefault();
});


// новый чат

const $searchListChannelCreate = document.querySelector('.search-list-vis-js'),
$searchListVisBg = document.querySelector('.search-list-vis-bg'),
$createModalChats = document.querySelector('.create-modal-chats');

$createModalChats.addEventListener('click', (e) => {
    e.preventDefault();
    $searchListChannelCreate.classList.add('active');
    calcHeightMessEnter();
});
$searchListVisBg.addEventListener('click', (e) => {
    
    $searchListChannelCreate.classList.remove('active');
});


// анонимный профиль 

const $typeAnonymousProfile = document.querySelector('.type-anonymous-profile'),
$rightChat = document.querySelector('.right-chat'),
$anonymousProfile = document.querySelector('.anonymous-profile');

$typeAnonymousProfile.addEventListener('click', e => {
    e.preventDefault();

    if ($typeAnonymousProfile.getAttribute('active')) {
        $rightChat.classList.remove('blur-chat');
        $anonymousProfile.classList.remove('active-flex');
        $typeAnonymousProfile.style.zIndex = '';
        $typeAnonymousProfile.removeAttribute('active');
    } else {
        $rightChat.classList.add('blur-chat');
        $anonymousProfile.classList.add('active-flex');
        $typeAnonymousProfile.style.zIndex = '2000';
        $typeAnonymousProfile.setAttribute('active', 'active');
    }
});


// создание групп

const $groupCreateJs = document.querySelector('.group-create-js'),
$userItemViewGroupJs = document.querySelector('.user-item-view-group-js'),
$listUserMomentJs = document.querySelector('.list-user-moment-js'),
$groupCreateJsState2 = document.querySelector('.group-create-js-state-2'),
$groupCreateJsStateBtn = document.querySelector('.group-create-js-state-btn'),
$userGroupActiveJs = document.querySelector('.user-group-active-js'),
$editGroupJs = document.querySelector('.edit-group-js'),
$viewGroupJs = document.querySelector('.view-group-js'),
$viewGroupEditJs = document.querySelector('.view-group-edit-js');

$userItemViewGroupJs.addEventListener('click', e => {
    e.preventDefault();
    $groupCreateJs.classList.add('active');
    $listUserMomentJs.classList.remove('active');
    $addedChatsAll.forEach(item => {
        item.classList.add('active');;
    });
});
$groupCreateJsStateBtn.addEventListener('click', e => {
    e.preventDefault();
    $groupCreateJs.classList.remove('active');
    $groupCreateJsState2.classList.add('active');


    
});
$userGroupActiveJs.addEventListener('click', e => {
    e.preventDefault();
    $viewGroupJs.classList.toggle('active');
    
});
$viewGroupEditJs.addEventListener('click', e => {
    e.preventDefault();
    $listUserMomentJs.classList.remove('active');
    $searchListChannelCreate.classList.toggle('active');
    $editGroupJs.classList.toggle('active');
    
});


// новый анонимный чат ( аватар )

const $newAnonymousJs = document.querySelector('.new-anonymous-js'),
$anonymousModeJs = document.querySelector('.anonymous-mode-js'),
$anonymousProfileJs = document.querySelector('.anonymous-profile-js');

$newAnonymousJs.addEventListener('click', e => {
    e.preventDefault();

    $anonymousProfileJs.classList.remove('active-flex');
    $anonymousModeJs.classList.add('active');
    
});

new Swiper('.swiper-container-share', {
    loop: false,

    slidesPerView: 4.1,

    grabCursor: true,
  
});

new Swiper('.swiper-container-profile-group', {
    loop: false,
    slidesPerView: 7.1,
});

new Swiper('.swiper-container-anonymous', {
    loop: false,
    slidesPerView: 7.1,
    centeredSlides: true,
      breakpoints: {
        // when window width is >= 320px
        620: {
        slidesPerView: 5,
        spaceBetween: 20
        },

        1000: {
        slidesPerView: 7.1,

        }
    }
});

calcHeightMessEnter();